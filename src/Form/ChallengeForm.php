<?php

namespace Drupal\letsencrypt_challenge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the letsencrypt_challenge module.
 */
class ChallengeForm extends ConfigFormBase {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new ChallengeForm form.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'challenge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['challenge'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Challenge string'),
      '#description' => $this->t("Challenge provided by Let's Encrypt. (This is saved as state, not config.)"),
      '#maxlength' => 512,
      '#size' => 128,
      '#default_value' => $this->state->get('letsencrypt_challenge.challenge', ''),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->state->set('letsencrypt_challenge.challenge', $form_state->getValue('challenge'));
  }

}
